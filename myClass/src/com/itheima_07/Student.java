package com.itheima_07;
/*
学生类
*/
public class Student {
    private String name;
    private int age;

    //构造方法（用来创建对象）
    public Student(){
        System.out.println("无参构造法");
    }

    public void show() {
        System.out.println(name + "," + age);
    }
}
