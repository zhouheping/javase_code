package com.itheima_03;
/*学生测试类*/
public class StudentTest01 {
        public static void main(String[] args) {
            //创建变量
            Student s = new Student();
            System.out.println(s);

            //使用对象
            System.out.println(s.name + "," + s.age);

            s.name = "张曼玉";
            s.age = 28;

            System.out.println(s.name + "," + s.age);

            s.study();
            s.doHomework();
        }
    }
