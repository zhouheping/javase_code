package com.itheima_08;

public class Student {
    private String name;
    private int age;

    //构造方法（用来创建对象）
/*
    public Student(){
        System.out.println("无参构造法");
    }
*/
    public Student(){

    }

    public Student(String name){
        this.name = name;
    }

    public Student(int age){
        this.age = age;
    }

    public Student(String name, int age){
        this.name = name;
        this.age = age;
    }

    public void show() {
        System.out.println(name + "," + age);
    }
}
