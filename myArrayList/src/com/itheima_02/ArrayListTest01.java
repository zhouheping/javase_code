package com.itheima_02;

import java.util.ArrayList;

public class ArrayListTest01 {
    public static void main(String[] args) {
        //创建集合对象
        ArrayList<String> array = new ArrayList<String>();

        //往集合中添加字符串对象
        array.add("刘正风");
        array.add("左冷禅");
        array.add("风清扬");

        //遍历集合，其次要能够获取到集合的长度，这个通过size()方法实现
        for(int i = 0; i < array.size(); i++){
            String s = array.get(i);//get(int index):获取集合中的每一个元素
            System.out.println(s);
        }
    }
}
