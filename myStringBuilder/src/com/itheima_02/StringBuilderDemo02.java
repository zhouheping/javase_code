package com.itheima_02;

public class StringBuilderDemo02 {
    public static void main(String[] args) {
        //StringBuilder 转为 String
//        StringBuilder sb = new StringBuilder();
//        sb.append("hello");

        //String s = sb; //这是个错误的做法

        //public String toString():通过toString()就可以实现把StringBuilder 转换为 String
       /*
       String s = sb.toString();
        System.out.println(s);
        */

        //String 转换为StringBuilder
        String s = "hello";

        //StringBuilder sb = s; //这是错误的做法

        //public StringBuilder(String s):通过构造方法就可以实现把 String 转换为 StringBuilder
        StringBuilder sb = new StringBuilder(s);
        System.out.println(sb);
    }
}
