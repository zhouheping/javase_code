package com.itheima_07;

public class MethodTest02 {
    public static void main(String[] args) {
        int[] arr = {12, 45, 98, 73, 60};
        int number = getMax(arr);
        System.out.println("数组中元素最大值为：" + number);
    }

    public static int getMax(int[] arr){
        int max = arr[0];
        for(int x=1; x<arr.length; x++){
            if(max < arr[x]){
                max = arr[x];
            }
        }
        return max;
    }
}
