package com.itheima_02;

public class MethodDemo {
    public static void main(String[] args) {
        isEvenNumber(20);
        int number = 20;
        isEvenNumber(number);
    }

    public static void isEvenNumber(int number){
        if(number%2 == 0){
            System.out.println(true);
        }else{
            System.out.println(false);
        }
    }
}
