package com.itheima_02;
/*
测试类
*/
public class AnimalDemo {
    public static void main(String[] args) {
        //有父类引用指向子类对象
         /*
         多态中成员访问特点
         成员变量：编译看左边，运行看左边
         成员方法：编译看左边，运行看右边
         */
        //不同之处：因为成员方法有重写，而成员变量没有
        Animal a = new Cat();
        System.out.println(a.age);
//      System.out.println(a.weight);

        a.eat();
//      a.playGame();
    }
}
