package com.itheima_03;

import java.util.Scanner;

public class StringTest05 {
    public static void main(String[] args) {
        //键盘录入一个字符串，用Scanner实现
        Scanner sc = new Scanner(System.in);

        System.out.println("请输入一个字符串：");
        String s = sc.nextLine();

        String ss = reverse(s);
        System.out.println(ss);
    }

    public static String reverse(String s){
        //在方法中把字符串倒着遍历，然后把每一个得到的字符拼接成一个字符串并返回
        String ss = "";
        for(int i = s.length()-1; i >=0 ; i--){
            ss += s.charAt(i);
        }
        return ss;
    }
}