package com.itheima;
import java.util.Scanner;

public class Test01 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("请输入星期数：");
        int week = sc.nextInt();

        if(week == 1){
            System.out.println("今天的减肥活动是跑步！");
        }else if(week == 2){
            System.out.println("今天的减肥活动是游泳！");
        }else if(week == 3){
            System.out.println("今天的减肥活动是慢走！");
        }else if(week == 4){
            System.out.println("今天的减肥活动是动感单车！");
        }else if(week == 5){
            System.out.println("今天的减肥活动是拳击！");
        }else if(week == 6){
            System.out.println("今天的减肥活动是爬山！");
        }else if(week == 7){
            System.out.println("今天要好好吃一顿！！！");
        }else{
            System.out.println("您输入的星期数有误！！！");
        }
    }
}
