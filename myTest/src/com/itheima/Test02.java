package com.itheima;
/*
1.手动导包：import java.util.Scanner;
2.快捷键导包：Alt+Enter
3.自动导包
*/
import java.util.Scanner;

public class Test02 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入星期数：");
        int week = sc.nextInt();

        switch (week){
            case 1:
                System.out.println("今天的减肥活动是跑步！");
                break;
            case 2:
                System.out.println("今天的减肥活动是游泳！");
                break;
            case 3:
                System.out.println("今天的减肥活动是慢走！");
                break;
            case 4:
                System.out.println("今天的减肥活动是动感单车！");
                break;
            case 5:
                System.out.println("今天的减肥活动是拳击！");
                break;
            case 6:
                System.out.println("今天的减肥活动是爬山！");
                break;
            case 7:
                System.out.println("今天要好好吃一顿！");
                break;
            default:
                System.out.println("您输入的星期数有误！！！");
        }
    }
}
