package com.itheima_03;
/*
    测绘类
*/
public class PersonDemo {
    public static void main(String[] args) {
        //创建对象
        PingPangPlayer ppp = new PingPangPlayer("王皓",30);
/*      ppp.setName("王皓");
        ppp.setAge(30);
        */
        System.out.println(ppp.getAge()+","+ppp.getName());
        ppp.eat();
        ppp.study();
        ppp.speak();
        System.out.println("-------------------");

        BasketballPlayer bp = new BasketballPlayer("姚明",35);
/*      bp.setName("姚明");
        bp.setAge(35);
        */
        System.out.println(bp.getAge()+","+bp.getName());
        bp.eat();
        bp.study();
    }
}
